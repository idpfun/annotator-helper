#!/usr/bin/python2.7

# config_reader - Parser for configuration file
# By XX
# Shared under XX

"""
Module:       config_reader
Description:  Parser for configuration file
Version:      -
Last Edit:    2018-12-11
Functions:
    isfloat
	load_configeration_options
Dependencies:
	General: os,inspect,ConfigParser
"""

################################################################################
## SECTION I: GENERAL SETUP & PROGRAM DETAILS                                 ##
################################################################################

# General imports
import os,inspect
# General imports: configuration file parser
from ConfigParser import SafeConfigParser

################################################################################
## END OF SECTION I                                                           ##
################################################################################

################################################################################
## SECTION II: FUNCTIONS                                                      ##
################################################################################

##----------------------------------------------------------------------------##
## isfloat function - check if value is float                                 ##
##----------------------------------------------------------------------------##
def isfloat(value):
	'''
	Returns True is value is float
	'''
	try:
		float(value)
		return True
	except ValueError:
		return False

##----------------------------------------------------------------------------##
## load_configeration_options function - Parse options from file              ##
##----------------------------------------------------------------------------##
def load_configeration_options(sections=[]):
	'''
	Read and parse options from Config File config.ini
	'''
	options = {}
	# Create object of class SafeConfigParser
	config = SafeConfigParser()

	# Define path to config file from caller
	config_path = os.path.abspath(os.path.join(os.path.dirname(inspect.stack()[0][1]),'./config.ini'))
	if os.path.exists(config_path):
		# Read config file
		config.read(os.path.join(os.path.dirname(inspect.stack()[0][1]),'./config.ini'))
		# Parse config file into options dict
		for each_section in config.sections():
			if len(sections) == 0 or each_section in sections:
				# Iterate over name,value pairs in option
				for (each_key, each_val) in config.items(each_section):
					each_val = each_val.strip()
					if each_val == "True" or each_val == "true":
						options[each_key] = True
					elif each_val == "False" or each_val == "false":
						options[each_key] = False
					elif isfloat(each_val):
						options[each_key] = float(each_val)
					else:
						options[each_key] = each_val
		# Iterate over options dict
		for option in options:
			# Check path exists in path-defining options
			if option[-4:] == 'path':
				if not os.path.exists(options[option]):
					print "In config.ini - " + option + "=" + options[option],"- path does not exist."
	else:
		print "Config File Path not found:",config_path
	
	# Return parsed options as dict
	return options
	
################################################################################
## END OF SECTION II                                                          ##
################################################################################